import {expect, test} from '@oclif/test'

describe('fund-bundlr', () => {
  test
  .stdout()
  .command(['fund-bundlr'])
  .it('runs hello', ctx => {
    expect(ctx.stdout).to.contain('hello world')
  })

  test
  .stdout()
  .command(['fund-bundlr', '--name', 'jeff'])
  .it('runs hello --name jeff', ctx => {
    expect(ctx.stdout).to.contain('hello jeff')
  })
})
