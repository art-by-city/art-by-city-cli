import {expect, test} from '@oclif/test'

describe('generate-previews', () => {
  test
  .stdout()
  .command(['generate-previews'])
  .it('runs hello', ctx => {
    expect(ctx.stdout).to.contain('hello world')
  })

  test
  .stdout()
  .command(['generate-previews', '--name', 'jeff'])
  .it('runs hello --name jeff', ctx => {
    expect(ctx.stdout).to.contain('hello jeff')
  })
})
