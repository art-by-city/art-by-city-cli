Art By City CLI
=================
<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
# 1) Install
$ npm install -g @artbycity/cli

# 2) Generate image preview assets
$ artbycity generate-previews [DIR]

# 3) Publish to Arweave via Bundlr
$ artbycity publish [DIR] -w [PATH_TO_SOL_WALLET] -t mainnet
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`artbycity generate-previews DIR`](#artbycity-generate-previews-dir)
* [`artbycity help [COMMAND]`](#artbycity-help-command)
* [`artbycity publish DIR`](#artbycity-publish-dir)

## `artbycity generate-previews DIR`

Generate image asset previews

```
USAGE
  $ artbycity generate-previews [DIR] [-f] [-s <value>] [-n <value>]

ARGUMENTS
  DIR  Top-level project directory containing an /images directory to generate
       /previews for

FLAGS
  -f, --force           force overwrite already generated preview images
  -n, --number=<value>  number of images to generate, will continue until image not found
  -s, --start=<value>   [default: 1] start id for images (e.g. 1 to start with images/1.png)

DESCRIPTION
  Generate image asset previews

EXAMPLES
  $ artbycity generate-previews ./project
```

_See code: [dist/commands/generate-previews.ts](https://github.com/jim-toth/art-by-city-cli/blob/v0.0.0/dist/commands/generate-previews.ts)_

## `artbycity help [COMMAND]`

Display help for artbycity.

```
USAGE
  $ artbycity help [COMMAND] [-n]

ARGUMENTS
  COMMAND  Command to show help for.

FLAGS
  -n, --nested-commands  Include all nested commands in the output.

DESCRIPTION
  Display help for artbycity.
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v5.1.12/src/commands/help.ts)_

## `artbycity publish DIR`

Publish collection to Arweave via Bundlr with Art By City Protocol tags

```
USAGE
  $ artbycity publish [DIR] -w <value> [-s <value>] [-t testnet|mainnet]

ARGUMENTS
  DIR  Top-level project directory

FLAGS
  -s, --start=<value>             [default: 1] starting token id to begin publishing with
  -t, --target=(testnet|mainnet)  [default: testnet] target environment to publish to
  -w, --wallet=<value>            (required) path to Solana wallet keyfile

DESCRIPTION
  Publish collection to Arweave via Bundlr with Art By City Protocol tags

EXAMPLES
  $ artbycity publish ./project -w [PATH_TO_SOL_KEYFILE] -t testnet
```

_See code: [dist/commands/publish.ts](https://github.com/jim-toth/art-by-city-cli/blob/v0.0.0/dist/commands/publish.ts)_
<!-- commandsstop -->
