import { Command, Flags } from '@oclif/core'
import * as fs from 'fs'
import Bundlr from '@bundlr-network/client'
import BundlrTransaction from '@bundlr-network/client/build/common/transaction'
import { Keypair } from '@solana/web3.js'

export default class Publish extends Command {
  static description =
    'Publish collection to Arweave via Bundlr with Art By City Protocol tags'

  static examples = [
    'artbycity publish ./project -w [PATH_TO_SOL_KEYFILE] -t testnet',
  ]

  static flags = {
    start: Flags.integer({
      char: 's',
      description: 'starting token id to begin publishing with',
      required: true
    }),
    target: Flags.enum({
      char: 't',
      description: 'target environment to publish to',
      options: [ 'testnet', 'mainnet' ],
      default: 'testnet'
    }),
    wallet: Flags.string({
      char: 'w',
      description: 'path to Solana wallet keyfile',
      required: true
    })
  }

  static args = [
    {
      name: 'dir',
      required: true,
      description: `Top-level project directory`
    }
  ]

  private jsonDir!: string
  private imagesDir!: string
  private imagesNBDir!: string
  private previewsDir!: string
  private jsonOutputDir!: string

  private bundlr!: Bundlr
  private wallet!: Uint8Array

  public async run(): Promise<void> {
    const { args, flags } = await this.parse(Publish)

    this.wallet = this.ensureWalletKey(flags.wallet)
    this.ensureInputDirectories(args.dir)
    this.ensureOutputDirectories(args.dir)
    this.bundlr = this.ensureBundlr(flags.target, this.wallet)

    const keypair = Keypair.fromSecretKey(this.wallet)

    const baseTags: { name: string, value: string }[] = [
      // Art By City Protocol
      { name: 'Protocol', value: 'ArtByCity' },
      { name: 'App-Name', value: 'ArtByCity' },
      { name: 'App-Version', value: 'cli-0.0.0' },

      // External Owner
      { name: 'External-Network', value: 'Solana' },
      { name: 'External-Owner', value: keypair.publicKey.toString() },
    ]

    let i = 1
    let _id = flags.start
    const manifestIds: string[] = []
    while (true) {
      const tokenId = _id
      const fileId = i
      const manifestPath = `${this.jsonDir}/${fileId}.json`
      if (!fs.existsSync(manifestPath)) {
        break
      }

      const {
        manifest,
        image,
        imageNB,
        smallPreview,
        hdPreview
      } = this.ensureTokenAssets(fileId)

      // Create and sign asset transactions
      const imageTx = this.bundlr.createTransaction(
        image,
        {
          tags: [
            ...baseTags,
            { name: 'Content-Type', value: 'image/png' }
          ]
        }
      )
      await imageTx.sign()

      const imageNBTx = this.bundlr.createTransaction(
        imageNB,
        {
          tags: [
            ...baseTags,
            { name: 'Content-Type', value: 'image/png' }
          ]
        }
      )
      await imageNBTx.sign()

      const smallPreviewTx = this.bundlr.createTransaction(
        smallPreview,
        {
          tags: [
            ...baseTags,
            { name: 'Content-Type', value: 'image/jpeg' }
          ]
        }
      )
      await smallPreviewTx.sign()

      const hdPreviewTx = this.bundlr.createTransaction(
        hdPreview,
        {
          tags: [
            ...baseTags,
            { name: 'Content-Type', value: 'image/jpeg' }
          ]
        }
      )
      await hdPreviewTx.sign()

      // Update manifest
      const baseURL = 'https://arweave.net'
      manifest.name = `Metazen #${tokenId}`
      manifest.symbol = 'MTZN'
      manifest.description = manifest.name
      manifest.image = `${baseURL}/${imageTx.id}`
      manifest.thumbnail_url = `${baseURL}/${smallPreviewTx.id}`
      manifest.external_url = 'https://metazensnft.com'
      manifest.tld = 'mtzn'
      manifest.properties = {
        category: 'image',
        creators: [
          {
            "address": "DVrQA96GwGtgY4nV5qsXDY2EyEYZPzpAj3otAanq7FMY",
            "share": 100
          }
        ],
        files: [
          {
            name: 'image',
            type: 'image/png',
            uri: manifest.image
          },
          {
            name: 'transparent',
            type: 'image/png',
            uri: `${baseURL}/${imageNBTx.id}`
          },
          {
            name: 'thumbnail',
            type: 'image/jpeg',
            uri: manifest.thumbnail_url
          },
          {
            name: 'preview',
            type: 'image/jpeg',
            uri: `${baseURL}/${hdPreviewTx.id}`
          }
        ]
      }

      // Create and sign manifest
      const manifestTx = this.bundlr.createTransaction(
        JSON.stringify(manifest, null, 2),
        {
          tags: [
            ...baseTags,
            { name: 'Category', value: 'Artwork' },
            { name: 'Content-Type', value: 'application/json' },
            { name: 'Collection', value: 'Metazens' },
            { name: 'Symbol', value: 'MTZN' },
            { name: 'Token-ID', value: tokenId.toString() },
            { name: 'Slug', value: tokenId.toString() },
            { name: 'Manifest-Format', value: 'ERC-1155' }
          ]
        }
      )
      await manifestTx.sign()

      this.log(`\tUploading manifest tx ${manifestTx.id}`)
      await this.upload(manifestTx)

      this.log(`\tUploading image tx ${imageTx.id}`)
      await this.upload(imageTx)

      this.log(`\tUploading imageNB tx ${imageNBTx.id}`)
      await this.upload(imageNBTx)

      this.log(`\tUploading small preview tx ${smallPreviewTx.id}`)
      await this.upload(smallPreviewTx)

      this.log(`\tUploading hd preview tx ${hdPreviewTx.id}`)
      await this.upload(hdPreviewTx)

      manifestIds.push(`https://arweave.net/${manifestTx.id}`)
      this.log(
        `\tPublished token ${tokenId}: https://arweave.net/${manifestTx.id}`
      )

      i++
      _id++
    }

    // Write manifest IDs to JSON file
    const manifestURIs = `${args.dir}/manifestURIs-${Date.now()}.json`
    fs.writeFileSync(manifestURIs, JSON.stringify(manifestIds, null, 2))
    this.log(`Saved token manifest URIs to ${manifestURIs}`)
  }

  private ensureWalletKey(walletPath: string): Uint8Array {
    if (!fs.existsSync(walletPath)) {
      this.error(`${walletPath} is not a valid path`)
    }
    try {
      const walletFile = fs.readFileSync(walletPath)
      const parsed = JSON.parse(walletFile.toString())
      return Uint8Array.from(parsed)
    } catch (err: any) {
      this.error(`Could not parse wallet keyfile: ${walletPath}\n${err.message}`)
    }
  }

  private ensureBundlr(target: string, wallet: Uint8Array) {
    const keypair = Keypair.fromSecretKey(wallet)
    this.log(`Using Bundlr on ${target} with ${keypair.publicKey}`)

    const host = target === 'mainnet'
      ? 'https://node1.bundlr.network'
      : 'https://devnet.bundlr.network'

    const config = target === 'mainnet'
      ? {}
      : {
          providerUrl: 'https://api.devnet.solana.com'
        }

    this.log('hi')
    return new Bundlr(host, 'solana', wallet, config)
  }

  private ensureInputDirectories(dir: string) {
    if (!fs.existsSync(dir)) {
      this.error(`${dir} is not a valid directory`)
    }

    this.jsonDir = `${dir}/json`
    if (!fs.existsSync(this.jsonDir)) {
      this.error(`${this.jsonDir} is not a valid directory`)
    }

    this.imagesDir = `${dir}/images`
    if (!fs.existsSync(this.imagesDir)) {
      this.error(`${this.imagesDir} is not a valid directory`)
    }

    this.imagesNBDir = `${dir}/imagesNB`
    if (!fs.existsSync(this.imagesDir)) {
      this.error(`${this.imagesNBDir} is not a valid directory`)
    }
  }

  private ensureOutputDirectories(dir: string) {
    this.previewsDir = `${dir}/previews`
    if (!fs.existsSync(this.previewsDir)) {
      this.error(
        `${this.previewsDir} not found, run generate-previews command first`
      )
    }
    if (!fs.existsSync(`${this.previewsDir}/small`)) {
      this.error(
        `${this.previewsDir}/small not found, run generate-previews command
        first`
      )
    }
    if (!fs.existsSync(`${this.previewsDir}/hd`)) {
      this.error(
        `${this.previewsDir}/hd not found, run generate-previews command first`
      )
    }

    this.jsonOutputDir = `${dir}/jsonUpdated`
    if (!fs.existsSync(`${this.jsonOutputDir}`)) {
      fs.mkdirSync(`${this.jsonOutputDir}`)
    }
  }

  private ensureTokenAssets(fileId: number) {
    // Manifest
    const manifestPath = `${this.jsonDir}/${fileId}.json`
    if (!fs.existsSync(manifestPath)) {
      this.error(`JSON Manifest not found: ${manifestPath}`)
    }
    this.log(`Found ${manifestPath}`)
    const json = fs.readFileSync(manifestPath).toString()
    const manifest = JSON.parse(json)

    // Image
    const imagePath = `${this.imagesDir}/${fileId}.png`
    if (!fs.existsSync(imagePath)) {
      this.error(`Image not found: ${imagePath}`)
    }
    this.log(`\tFound ${imagePath}`)
    const image = fs.readFileSync(imagePath)

    // Image no background
    const imageNBPath = `${this.imagesNBDir}/${fileId}.png`
    if (!fs.existsSync(imageNBPath)) {
      this.error(`Transparent image not found: ${imageNBPath}`)
    }
    this.log(`\tFound ${imageNBPath}`)
    const imageNB = fs.readFileSync(imageNBPath)

    // Small preview
    const smallPreviewPath = `${this.previewsDir}/small/${fileId}.jpg`
    if (!fs.existsSync(smallPreviewPath)) {
      this.error(`Small preview image not found: ${smallPreviewPath}`)
    }
    this.log(`\tFound ${smallPreviewPath}`)
    const smallPreview = fs.readFileSync(smallPreviewPath)

    // HD preview
    const hdPreviewPath = `${this.previewsDir}/hd/${fileId}.jpg`
    if (!fs.existsSync(hdPreviewPath)) {
      this.error(`HD preview image not found: ${hdPreviewPath}`)
    }
    this.log(`\tFound ${hdPreviewPath}`)
    const hdPreview = fs.readFileSync(hdPreviewPath)

    return { manifest, image, imageNB, smallPreview, hdPreview }
  }

  private async upload(tx: BundlrTransaction) {
    // const price = await this.bundlr.getPrice(tx.data.length)
    // const balance = await this.bundlr.getLoadedBalance()

    // if (balance.isGreaterThan(price)) {
    //   this.log(`Balance (${balance}) is less than price (${price})`)
    //   const addFunds = balance.minus(price).multipliedBy(1.1)
    //   await this.bundlr.fund(addFunds)
    // }

    await tx.upload()
  }
}


