import { Command, Flags } from '@oclif/core'
import * as fs from 'fs'
import sharp from 'sharp'

export default class GeneratePreviews extends Command {
  static description = 'Generate image asset previews'

  static examples = [
    'artbycity generate-previews ./project',
  ]

  static flags = {
    force: Flags.boolean({
      char: 'f',
      default: false,
      description: 'force overwrite already generated preview images'
    }),
    start: Flags.integer({
      char: 's',
      default: 1,
      description: 'start id for images (e.g. 1 to start with images/1.png)'
    }),
    number: Flags.integer({
      char: 'n',
      description:
        'number of images to generate, will continue until image not found'
    })
  }

  static args = [
    {
      name: 'dir',
      required: true,
      description: `
        Top-level project directory containing an /images directory to generate
        /previews for
      `
    }
  ]

  public async run(): Promise<void> {
    const { args, flags } = await this.parse(GeneratePreviews)

    if (!fs.existsSync(args.dir)) {
      this.error(`${args.dir} is not a valid directory`)
    }

    const imagesDir = `${args.dir}/images`
    if (!fs.existsSync(imagesDir)) {
      this.error(`${imagesDir} is not a valid directory`)
    }

    const previewsDir = `${args.dir}/previews`
    if (!fs.existsSync(previewsDir)) {
      fs.mkdirSync(previewsDir)
    }

    const sizes: any = {
      'small': {
        w: 350,
        h: 350
      },
      'hd': {
        w: 1080,
        h: 1080
      }
    }

    for (const size in sizes) {
      if (!fs.existsSync(`${previewsDir}/${size}`)) {
        fs.mkdirSync(`${previewsDir}/${size}`)
      }
    }

    let i = 0
    while (!flags.number || flags.number && i < flags.number) {
      const id = flags.start + i
      const input = `${imagesDir}/${id}.png`

      if (!fs.existsSync(input)) {
        break
      }

      this.log(`Found ${input}`)

      const small = `${previewsDir}/small/${id}.jpg`
      if (!flags.force && fs.existsSync(small)) {
        this.log(`\tFound ${small}, skipping`)
      } else {
        this.log(`\tGenerate previews for ${input}`)
        for (const size in sizes) {
          await sharp(input)
            .resize(sizes[size].w, sizes[size].h, {
              fit: sharp.fit.inside,
              withoutEnlargement: true
            })
            .jpeg({
              quality: 100,
              chromaSubsampling: '4:4:4'
            })
            .toFile(`${previewsDir}/${size}/${id}.jpg`)
        }
      }

      i++
    }

    this.log(`Generated previews for ${i} images to ${previewsDir}`)
  }
}


